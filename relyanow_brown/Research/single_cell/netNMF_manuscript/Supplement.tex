\documentclass[12pt]{article}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{color}
\PassOptionsToPackage{hyphens}{url}\usepackage{hyperref}

%% make sure you have the nature.cls and naturemag.bst files where
%% LaTeX can find them


\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{cleveref}

\usepackage{natbib}
\bibliographystyle{plainnat}

\newcommand{\ourmethod}{netNMF-sc } 
% color text for individual authors, update with individual names
\newcommand{\ben}{\textcolor{red}}
\newcommand{\rebecca}{\textcolor{blue}}
\newcommand{\todo}{\textcolor{magenta}}
\renewcommand{\thepage}{S\arabic{page}}  
\renewcommand{\thesection}{S\arabic{section}}   
\renewcommand{\thetable}{S\arabic{table}}   
\renewcommand{\thefigure}{S\arabic{figure}}
\title{Supplement to netNMF-sc: Leveraging gene-gene interactions for imputation and dimensionality reduction in single-cell expression analysis}
\begin{document}
\date{}
\maketitle

\section{netNMF-sc with Euclidean distance}\label{euclidean}
\rebecca{
We have also formulated netNMF-sc with Euclidean distance instead of KL divergence. This assumes that the data is drawn from an underlying Gaussian distribution rather than a Poisson distribution. This (1) requires the use of log-normalized data and (2) requires that we account for zero-inflation in the data. We propose the following formula below.
\begin{equation}\label{netNMF-sc}
	\min_{\mathbf W\geq 0, \mathbf H \geq 0} ||\mathbf{M \circ X}-\mathbf{M \circ WH}||_F^2 + \lambda Tr(\mathbf W^T\mathbf{LW}), 
\end{equation}
where $\lambda$ is a positive real constant,  $\mathbf{L}$ is the Laplacian matrix of the gene-gene interaction network, $\circ$ indicates element-wise multiplication (or Schur product of matrices), and $Tr(\cdot)$ indicates the trace of the matrix. We include zero inflation using a binary matrix $\mathbf{M}$ that masks zero entries in $\mathbf{X}$,
such that a non-zero entry in $a_{ij}$ in $\mathbf{WH}$ is not penalized when the corresponding entry $x_{ij}$ of $\mathbf{X}$ is equal to $0$.
}

\rebecca{
\section{Selecting $\lambda$ via cross-validation}\label{lambda_select}
We follow the following procedure for selection of the regularization parameter $\lambda$ via cross-validation on held-out data:
\begin{enumerate}
	\item Select $20\%$ of the data to be held-out at random, let $v$ contain the indices of these data in $X$.
	\item Run netNMF-sc for a range of values $\lambda=[0,1,5,10,20,50,100]$, masking out held-out entries
	\item Calculate root mean square error (RMSE) between the held-out data from $X$ and the reconstructed data $\mathbf WH$, $\sqrt{\frac{\sum_{ij \in v} (WH_{ij} -  X_{ij})^2}{|v|}}$, where $|v|$ denotes the number of held-out entries
	\item Select the value of $\lambda$ which results in the lowest RMSE
	\end{enumerate} 
The range of values can be tuned for tuned to desired specificity, but we found that this coarse selection resulted in reasonable results on the tested datasets.}

\section{Library size normalization}\label{normalize}
For a transcript count matrix $\mathbf X$, the library size $l_j$ of each cell $j$ is the sum of all transcript counts across every gene,
$$l_j = \sum_{i \in n} x_{ij}.$$
To normalize $\mathbf X$, we divide each entry $x_{ij}$ in a cell's expression profile by the cell's library size and then multiply $x_{ij}$ by the median library size $q$ across all cells,
$$\bar x_{ij}  = q \frac{x_{ij}}{l_j},$$
where $\bar x_{ij}$ is an entry in the normalized transcript count matrix $\bar{\mathbf X}$.


\section{Data simulation}\label{simulation}
	We use a real gene-gene co-expression network obtained from Coexpedia \citep{yang2016coexpedia} and randomly select 5000 genes to be retained. To define differentially expressed genes, for each of the $k$ clusters, we randomly sample $5$ genes and their neighbors to be differentially expressed. If this results in more than $10\%$ of genes being differentially expressed in each cluster, we downsample, at random, these selected genes such that at most $10\%$ of the genes in each cluster are differentially expressed. Each differentially expressed gene is scaled by a \textit{differential expression factor} as described by Splatter \citep{zappia2017splatter}, however we ensure that if a gene is overexpressed in a cluster (differential expression factor $>1$), then its selected neighbors are also overexpressed. The same is true for for underexpressed genes (differential expression factor $<1$). Dropout of transcripts is performed following either the double exponential or the multinomial dropout model.



\begin{table}
\begin{center}
\caption {Methods for analyzing scRNA-seq data} 
\todo{[update this to include recent methods scNBMF and pCMF]}
\label{methodtable}
\begin{tabular}{|l|l|l|l|l|}
\hline
Method          & Imputation   & \shortstack{Dimensionality \\ reduction} & Clustering   & \shortstack{GRN \\ inference}  \\ \hline
BISCUIT \citep{azizi2017bayesian}         & $\checkmark$ &                          & $\checkmark$ &               \\ \hline
MAGIC \citep{van2018recovering}         & $\checkmark$ &                          &              &               \\ \hline
scImpute \citep{li2018accurate}        & $\checkmark$ &                          &              &               \\ \hline
drImpute \citep{Gong2018}       & $\checkmark$ &                          &              &               \\ \hline
SAVER \citep{huang2018saver}           & $\checkmark$ &                          &              &               \\ \hline
CIDR \citep{lin2017cidr}            &              & $\checkmark$             & $\checkmark$ &               \\ \hline
SC3 \citep{kiselev2017sc3}            &              &                          & $\checkmark$ &               \\ \hline
Seurat \citep{butler2018integrating}        &              &                          & $\checkmark$ &               \\ \hline
BackSPIN \citep{zeisel2015cell}        &              &                          & $\checkmark$ &               \\ \hline
PhenoGraph \citep{levine2015data}     &              &                          & $\checkmark$ &               \\ \hline
ZIFA \citep{pierson2015zifa}           &              & $\checkmark$             &              &               \\ \hline
ZINB-WaVE \citep{risso2018general}       & $\checkmark$ & $\checkmark$             &              &               \\ \hline
SIMLR \citep{wang2017visualization}           &              & $\checkmark$             &              &               \\ \hline
\textbf{netNMF-sc} & $\checkmark$ & $\checkmark$             &              &               \\ \hline
SCODE \citep{matsumoto2017scode}           &              &                          &              & $\checkmark$  \\ \hline
Sinova \citep{li2016systematic}         &              &                          &              & $\checkmark$  \\ \hline
SINCERA  \citep{guo2015sincera}       &              &                          & $\checkmark$ & $\checkmark$  \\ \hline
SCENIC \citep{aibar2017scenic}         &              &                          & $\checkmark$ & $\checkmark$  \\ \hline
\end{tabular}
\end{center}
\end{table}



%%%
 \begin{figure}
 \begin{center}
 \includegraphics[scale=.6]{figures/optimizers.pdf} % this command will be ignored
 \caption{(a-b) Adjusted rand index (ARI) and Root mean square error(RMSE) of netNMF-sc on simulated data with and without masking of zero entries. (c-d) Clustering performance (ARI) and imputation error (RMSE) of netNMF-sc with different optimizers (Adam, Momentum, Gradient descent, and Adagrad).}
 \label{optimizers}
 \end{center}
 \end{figure}
 %%%
 
 %%%
 \begin{figure}
 \begin{center}
 \includegraphics[scale=.6]{figures/time} % this command will be ignored
 \caption{Runtime of imputation methods as a function of size of input matrix $\mathbf X$.}
 \label{time}
  \end{center}
 \end{figure}
 %%%
% 
  %%%
 \begin{figure}
 \begin{center}
 \includegraphics[scale=.5]{figures/RMSE_multinomial.pdf} % this command will be ignored
   \end{center}
 \caption{Root mean square error (RMSE) for simulated data. Dropout is simulated in the \emph{multinomial dropout model}. (a) RMSE for different scRNA-seq methods on simulated data with different dropout rates. Results are an average across 20 simulations. (b) RMSE at $70\%$ dropout across 20 simulations.}
 \label{imputation_error}

 \end{figure}
 %%%
%%%
 \begin{figure}
 \includegraphics[scale=.5]{figures/netNMF_random_edges.pdf} % this command will be ignored
 \caption{Clustering performance of netNMF-sc run on simulated data with 5000 genes, 1000 cells, and 6 clusters. Dropout was simulated using the multinomial dropout model with a dropout rate of $0.7$. The x-axis measures the number of random edges added to the original graph $G = (V,E)$, where the number of random edges is $x|E|$. The red line shows the performance of NMF on the same data.} 
 \label{add_edges}
 \end{figure}
 %%%
 
 
% 
%%%%
% \begin{figure}
% \includegraphics[scale=.5]{figures/Sim_exp_dropout.pdf} % this command will be ignored
% \caption{Clustering results for simulated data with different dropout rates. Dropout is simulated with the double exponential dropout model. (a-d) Clustering metrics vs. dropout rate, averaged over 20 simulations. (e-h) Violin plots of clustering metrics at $70\%$ dropout. }
% \label{simulated}
% \end{figure}
% %%%
 
  %%%
 \begin{figure*}
 \includegraphics[scale=.7]{figures/simulated_double_exp.pdf} % this command will be ignored
 \caption{Comparison of netNMF-sc and other methods on clustering cell types in a dataset containing $1000$ cells and $5000$ genes, with dropout simulated using the \emph{double exponential dropout model}. (a) Clustering results for several scRNA-seq methods on simulated data with different dropout rates. (b) Imputation results with different dropout rates. (c) 2D tSNE projections of imputed data at $70\%$ dropout.  (d) Distribution of adjusted rand index (ARI) at $70\%$ dropout over $20$ simulations. (e) Distribution of root mean squared error (RMSE) at $70\%$ dropout over $20$ simulations.}
 \label{simulated_mix}
 \end{figure*}
 %%% 
 
  %%%
 \section{Clustering on cell cycle data}
 To quantify the effect our choice of network has on the performance of netNMF-sc, we ran netNMF-sc with two different external networks as well as a network containing randomized edges. The first network is the previously described network obtained from the ESCAPE database \citep{xu2014construction}. The second network is a generic gene-gene co-expression network which is the result of combining expression data from $2486$ mouse microarray experiments \citep{yang2016coexpedia}. Next, we constructed a $k$-nearest neighbors network, constructed by representing the 10 nearest neighbors of each gene in the input data matrix as edges with weight $1$ in the network. Finally, we constructed a randomized network that maintains the same node degree as the ESCAPE network by performing the double\_edge\_swap procedure from the python library networkx. 

We found that all networks besides the random network significantly improved clustering results compared to NMF (Figure \ref{Buettner_supplement}(f)), with the mESC-specific network obtained from the ESCAPE database performing the best. We note that the random network has slightly worse average performance compared to NMF (ARI = $0.64$ compared to $0.67$) however the variance of the results is significantly smaller. This indicates that a random network may introduce some sparsity constraints (similar to L1 regularization) but does not provide significant increase in clustering performance.


 \begin{figure*}
 \begin{center}
 \includegraphics[scale=.5]{figures/Buettner.pdf} % this command will be ignored
 \end{center}
 \caption{Clustering results for cell cycle data from \citep{buettner2015computational}. (a) 2D and 3D t-SNE projections of imputed scRNa-seq data using different methods. (b-e) Clustering statistics (ARI, NMI, Jaccard, and Purity) for $k$-means clustering applied to cell cycle data imputed using MAGIC, scImpute, NMF, or netNMF-sc as well as k-means clustering applied to the raw dataset and the clusters obtained from CIDR. (f) NMF is compared with netNMF-sc run with different networks used as input. Coexpedia is a generic gene-gene co-expression network, ESCAPE is a gene-gene co-expression network specific to mESCs, and KNN is a $k$-nearest neighbors network constructed from the 10 nearest neighbors of each gene in the input data matrix. Random is a random network constructed to have the same number of edges and degree as the ESCAPE network.}
 \label{Buettner_supplement}
 \end{figure*}
 
 \newpage


 \section{Cell Cycle Correlation Analysis}
 To assess the effect of the input network $\mathbf{G}$ on gene-gene correlations in the imputed data matrix $\mathbf{WH}$ we evaluated the overlap between pairs of well-correlated genes in $\mathbf{WH}$ and gene-gene edges in the input network $\mathbf{G}$. For this analysis our input matrix $\mathbf{X}$ was obtained from \cite{buettner2015computational} and the input network $\mathbf{G}$ from \cite{xu2014construction}. We constructed correlation matrices $C(\mathbf{WH})$ and $C(\mathbf{X})$ of the imputed and raw count matrix respectively, where an entry $a_{ij}$ in $C(\mathbf{X})$ is the Pearson's correlation coefficient between genes $i$ and $j$ in $\mathbf X$ (and similarly for $C(\mathbf{WH})$). As we expected, we found greater enrichment (Fisher's Exact test, $p = 1.4 \times 10^{-127}$) for gene-gene edges present $\mathbf G$ in the top gene-gene correlations in $C(\mathbf{WH})$  than in the top gene-gene correlations in $C(\mathbf{X})$ (Fisher's Exact test, $p = 3.2 \times 10^{-113}$). However, the top correlations in $C(\mathbf X)$ were significantly enriched for network edges (Fisher's Exact test, $p = 3.2 \times 10^{-113}$), suggesting that netNMF-sc did not completely alter the correlations, but boosted a signal already present in the data (Figure \ref{mESC}).
 
  
  \begin{figure*}[]
  \begin{center}
 \includegraphics[scale=.6]{figures/correlation_supplement.pdf} % this command will be ignored
 \end{center}
 \caption{(a) Average $R^2$ correlation over gene pairs on permuted cell cycle data as a function of the number $d$ of dimensions in the matrix factorization from netNMF-sc. (b) Average $R^2$ correlation over gene pairs on permuted cell cycle data as a function of the diffusion operator, $t$, used by MAGIC (light blue indicates standard deviation). $t=5$ is auto-selected by MAGIC according to the Procrustes disparity of the diffused data. (c) netNMF-sc run on random data drawn from $N(2,2)$. (d) MAGIC run on random data drawn from $N(2,2)$.}
 \label{correlation}
 \end{figure*}
 
   \begin{figure*}[]
  \begin{center}
 \includegraphics[scale=.6]{figures/MAGIC_sim.pdf} % this command will be ignored
 \end{center}
 \caption{Gene-gene correlations introduced by MAGIC on expression matrices simulated from a $N(2,2)$ distribution.}
 \label{MAGIC}
 \end{figure*}
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[tbp]
\begin{center}
 \includegraphics[scale=.75]{figures/enrichment.pdf}
  \end{center}
 \caption{Enrichment of the imputed count matrix $\mathbf{WH}$ (blue) and the raw count matrix $\mathbf X$ for edges in the input network (ESCAPE).}
 \label{mESC}
 \end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 \section{Recovering marker genes and gene-gene correlations from EMT data}
 Using a set of $16$ canonical EMT marker genes ($3$ genes overexpressed in epithelial cells and $13$ genes overexpressed in mesenchymal cells) \citep{gibbons2018pan}, we defined the set of all $120$ gene pairs as our gold standard. We note that this set includes several gene pairs not investigated in the MAGIC paper \citep{van2017magic}. To validate our approach, we looked for positive correlations between pairs of mesenchymal or epithelial genes and negative correlations between pairs containing one epithelial and one mesenchymal gene. 
 
 We clustered cells by \emph{CDH1} and \emph{VIM} expression, two canonical marker genes for epithelial (\emph{CDH1}) and mesenchymal cells (\emph{VIM}), respectively. We labeled the $200$ cells with the highest \emph{CDH1} expression epithelial and the $200$ cells with the highest \emph{VIM} expression mesenchymal. We compared the ranked list of differentially expressed genes from data imputed by netNMF-sc to the ranked lists of differentially expressed genes from the raw data and data imputed NMF, MAGIC, scImpute.
We found that the EMT marker genes ranked very highly in netNMF-sc results ($p \leq 1.4 \times 10 ^{-5}$, Wilcoxon rank sum), a significant improvement compared to their ranking in the raw data ($p \leq 3.1 \times 10^{-3}$, Wilcoxon rank sum) (Figure \ref{EMT}(a)). In contrast, the next best performing method MAGIC had a smaller improvement in the ranking of EMT marker genes compared to the raw data ($p \leq 1.1 \times 10^{-4}$, Wilcoxon rank sum).

We observed that in data imputed by MAGIC, the E marker gene \textit{TJP1} had higher average expression in M cells than E cells ($p=1.5 \times 10^{-33}$) (Figure \ref{EMT}(b)). This resulted in \textit{TJP1} being negatively correlated ($R=-0.57,p=3.4 \times 10^{-50}$) with another E marker gene, \textit{DSP}  in the MAGIC imputed data; in contrast, these E marker genes showed positive correlation ($R=0.66,p=6.4 \times 10^{-78}$) in the netNMF-sc imputed data, correlation that was not apparent in the raw data (Figure \ref{EMT}(c)). We also investigated whether netNMF-sc could recover gene-gene correlations between EMT marker genes in E and M cells. We expect that pairs of E or M genes would exhibit positive correlation, while pairs containing one E and one M gene would exhibit negative correlations. In data imputed by netNMF-sc, $12\%$ of the EMT gene pairs were significantly correlated ($R^2\geq 0.8,p \leq 2.2 \times 10^{-16}$), with all gene pairs correlated in the expected orientation (Figure \ref{EMT_table}). In data imputed by MAGIC, $23\%$ of EMT gene pairs were significantly correlated, but $5\%$ were correlated in the \emph{opposite} direction than expected (Figure \ref{EMT}(d)).


 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[]
\begin{center}
 \includegraphics[scale=.5]{figures/correlation_results_EMT2.pdf}
  \end{center}
 \caption{Comparison of gene-gene correlations and differential gene expression in raw data from \cite{van2017magic} and data imputed using \ourmethod, NMF, scImpute, and MAGIC. (a) Overlap between differentially expressed genes and EMT marker genes (log $p$-values from Fisher's exact test). (b) Expression of the E marker gene \emph{TJP1} in cells labeled as E (blue) and cells labeled as M (green) in data imputed by each method. In \ourmethod\ inputed data, \emph{TJP1} is overexpressed in E cells compared to M cells ($p=1.4 \times 10^{-12}$), as expected.  In contrast, in data imputed by MAGIC,  \emph{TJP1} is \emph{underexpressed} in E cells compared to M cells ($p=1.5 \times 10^{-33}$), and shows no significant difference in expression in raw and scImpute data. (c) Correlation between pairs of periodic genes in cell cycle data. (d) Scatter plot of two E phase genes: \textit{DSP} and \textit{TJP1}. The genes are positively correlated in data imputed by netNMF-sc ($p=6.3 \times 10^{-78}$) but negatively correlated in data imputed by MAGIC ($p=3.4 \times 10^{-50}$).}
 \label{EMT}
 \end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[tbp]
\begin{center}
 \includegraphics[scale=.65]{figures/correlation_results_EMT_table.pdf}
  \end{center}
 \caption{Fraction of all gene pairs and EMT gene pairs (defined by \cite{gibbons2018pan}) with significant correlations ($R^2 \geq 0.8,p \leq 2.2 \times 10^{-16}$) in the EMT dataset. \textit{Correct} orientation means that a pair of E-E or M-M genes have positive correlation while E-M genes have negative correlation.}
 \label{EMT_table}
 \end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%To assess the effect of input network $\mathbf{G}$ on the gene-gene correlations in the imputed data matrix, $C(\mathbf{WH})$, from real EMT data, we evaluated the overlap of well-correlated genes in $\mathbf{WH}$ and edges in the input network $\mathbf{G}$ obtained from \cite{yang2016coexpedia}. We constructed correlation matrices $C(\mathbf{WH})$ and $C(\mathbf{X})$ of the imputed and original count data respectively, where an entry $a_{ij}$ in $C(\mathbf{X})$ is again the Pearson's correlation coefficient between genes $i$ and $j$ in $\mathbf X$. Similar to the cell cycle data, we found greater enrichment (Fisher's Exact test, $p = 4.5e^{-157}$) for gene-gene edges present in $\mathbf G$ in the top gene-gene correlations in $C(\mathbf{WH})$  than in the top gene-gene correlations in $C(\mathbf{X})$ (Fisher's Exact test, $p = 1.5e^{-510}$). However, the top correlations in $C(\mathbf X)$ were significantly enriched for network edges (Fisher's Exact test, $p = 1.5e^{-150}$), suggesting that netNMF-sc did not completely alter the correlations, but boosted a signal already present in the data (Figure \ref{EMT}(e)).


 \newpage
 \newpage
 %%%
\bibliographystyle{naturemag}
\bibliography{sample}
\end{document}